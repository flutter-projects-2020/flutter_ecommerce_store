import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

main() {
  runApp(MaterialApp(
    home: HomePage(),
  ));
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: Text("ShopApp"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.white,
            ),
            onPressed: () {},
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.red),
              accountName: Text("Satya Prakash"),
              accountEmail: Text("satya.tryme@gmail.com"),
              currentAccountPicture: GestureDetector(
                child: CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(Icons.person),
                ),
              ),
            ),
            _DrawerButton(title: "Home Page", icon: Icons.home, color: Colors.brown, onTap: () {}),
            _DrawerButton(title: "My account", icon: Icons.person, color: Colors.pinkAccent, onTap: () {}),
            _DrawerButton(title: "My orders", icon: Icons.shopping_basket, color: Colors.green, onTap: () {}),
            _DrawerButton(title: "Categories", icon: Icons.dashboard, color: Colors.deepOrangeAccent, onTap: () {}),
            _DrawerButton(title: "Favourites", icon: Icons.favorite, color: Colors.red, onTap: () {}),
            Divider(),
            _DrawerButton(title: "Settings", icon: Icons.settings, color: Colors.blue, onTap: () {}),
            _DrawerButton(title: "About", icon: Icons.help, color: Colors.grey, onTap: () {}),
          ],
        ),
      ),
      body: ListView(
        children: <Widget>[
          _ImageCarousel(),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(child: Text('Categories')),
          ),
          _HorizontalList(),
        ],
      ),
    );
  }
}

class _DrawerButton extends StatelessWidget {
  final VoidCallback onTap;
  final IconData icon;
  final Color color;
  final String title;
  _DrawerButton({this.onTap, this.color, this.title, this.icon});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: ListTile(
        title: Text(title),
        leading: Icon(icon, color: color),
      ),
    );
  }
}


class _ImageCarousel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200.0,
      child: Carousel(
        dotSize: 4.0,
        indicatorBgPadding: 2.0,
        boxFit: BoxFit.cover,
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(milliseconds: 1000),
        images: [
          Image.asset('images/c1.jpg'),
          Image.asset('images/m1.jpg'),
          Image.asset('images/m2.jpg'),
          Image.asset('images/w3.jpg'),
          Image.asset('images/w4.jpg'),
          Image.asset('images/w1.jpg'),
        ],
      ),
    );
  }
}

class _HorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          _Category(
            image_location: 'images/cats/tshirt.png',
            image_caption: 'T-shirts'
          ),
        ],

      ),
    );
  }
}

class _Category extends StatelessWidget {
  final String image_location;
  final String image_caption;
  _Category({this.image_location, this.image_caption});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: InkWell(
        onTap: () {},
        child: Container(
          width: 100.0,
          child: ListTile(
            title: Image.asset(image_location),
            subtitle: Text(image_caption),
          ),
        ),
      ),
    );
  }
}
